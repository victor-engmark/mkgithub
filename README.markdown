[![Build Status](https://gitlab.com/victor-engmark/mkrepo/badges/master/build.svg)](https://gitlab.com/victor-engmark/mkrepo)

mkrepo
======

Create a local GitHub repository with a single command.

Test
----

Requires `shunit2`.

    make test

Installation
------------

    git clone --recurse-submodules git@gitlab.com:victor-engmark/mkrepo.git
    cd mkrepo
    make test
    sudo make install

To install it in another directory:

    make install PREFIX=/some/path

Usage
-----

    mkrepo.sh [<options>] [directories]

Options
-------

    -c, --configure
           Write the options in this command as the new configuration and
           exit. If run as root, it writes to /etc/mkrepo.conf, otherwise
           it writes to ~/.mkrepo.

    -g, --git
           Use git:// read-only remote URL.

    -h, --https
           Use https:// remote URL.

    -s, --ssh
           Use ssh:// remote URL (default).

    -u, --user=username
           GitHub username. Default your github.user configuration value.

    --help
           Display this information and quit.

    -v, --verbose
           Verbose output.

Examples
--------

    mkrepo.sh -ch
           Configure mkrepo to use HTTPS remote URLs.

    mkrepo.sh ~/dev/mkrepo
           Make ready for your own mkrepo clone :)
